package com.igroup.advancedviews;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.Button;

/**
 * Created by igor on 11.01.16.
 */
public class AdvancedButton extends Button {

    public AdvancedButton(Context context) {
        super(context);
    }

    public AdvancedButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        parseAttributeSet(attrs);
        setFont();
    }

    public AdvancedButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        parseAttributeSet(attrs);
        setFont();
    }

    public AdvancedButton(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        parseAttributeSet(attrs);
        setFont();
    }

    private String mFont = null;

    protected void parseAttributeSet(AttributeSet attrs) {
        TypedArray a = getContext().getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.AdvancedViews,
                0, 0);
        try {
            mFont = a.getString(R.styleable.AdvancedViews_font);
        } finally {
            a.recycle();
        }
    }

    protected final void setFont() {
        if(mFont == null) return;
        Typeface ttf = Typeface.createFromAsset(getContext().getAssets(), mFont);
        setTypeface(ttf);
    }

}
