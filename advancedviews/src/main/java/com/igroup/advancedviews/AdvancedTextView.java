package com.igroup.advancedviews;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by igor on 11.01.16.
 */
public class AdvancedTextView extends TextView {

    private static final String TAG = AdvancedTextView.class.getSimpleName();

    public AdvancedTextView(Context context) {
        super(context);
    }

    public AdvancedTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        parseAttributeSet(attrs);
        setFont();
    }

    public AdvancedTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        parseAttributeSet(attrs);
        setFont();
    }

    public AdvancedTextView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        parseAttributeSet(attrs);
        setFont();
    }

    private String mFont = null;

    protected void parseAttributeSet(AttributeSet attrs) {
        TypedArray a = getContext().getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.AdvancedViews,
                0, 0);
        try {
            mFont = a.getString(R.styleable.AdvancedViews_font);
        } finally {
            a.recycle();
        }
    }

    protected final void setFont() {
        if(mFont == null) return;
        Typeface ttf = Typeface.createFromAsset(getContext().getAssets(), mFont);
        setTypeface(ttf);
    }
}
